
export default {
  mode: 'universal',
  head: {
    title: 'Adam Martyński · CV',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'robots', content: 'none' },
      { hid: 'description', name: 'description', content: 'Full stack developer from Cracow, Poland' }
    ]
  },
  css: [
    '@/assets/main.css'
  ],
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/markdownit'
  ],
  markdownit: {
    use: [
      'markdown-it-attrs'
    ]
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/google-analytics'
  ],
  googleAnalytics: {
    id: 'UA-145392542-1'
  }
}

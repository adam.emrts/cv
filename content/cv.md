# **Adam Martyński** {#name}
### **Full stack & app developer** `CV` {#role}

## **Contact**
#### Mail: [adam@emrts.xyz](mailto:adam@emrts.xyz) {.contact}
#### SMS: [+48 690 697 650](tel:+48690697650) {.contact}
#### Location: **Cracow, Poland** {.contact}

## **Bio** {#bio}
I'm a 33 years old full stack developer from Cracow. I’m truly interested in internet & computer technologies.

I've made first hosted website in 2001 and since 2003 I've got over 5 years experience in freelance graphic & web design; including designing posters, flyers, clothes prints, industrial designs & website themes. In this time I was mainly focused around learning web development know-how.

Since 2009 spent 4 years working as freelance web designer and developer; including building, theming and setting up many sites on variety of CMS's, improving my HTML & CSS skills, learning and understanding Javascript libraries and Ecmascript programming language itself.

Since June 2013 working as front-end developer; including building responsive & interactive websites and applications layouts; expanding knowledge about front-end javascript frameworks as AngularJS and Vue.js.

In 2015 I've shipped my first Node.js sever-side application and since then was working as full stack & app developer.

## **Education** {#education}
- 2009 - 2011 · Cracow University of Technology · `Physics`
- 2004 - 2008 · Cracow Technical College · `Telecommunication Technician`

## **Experience** {#experience}
- 2020 - now · [Sagasystem Tenix](https://tenix.eu) · `App Developer`
- 2019 - 2021 · [StageEye](https://stageeye.io) · `App Developer`
- 2017 - 2020 · [Tribe84 Records](https://www.tribe84records.com) · `Back-end & technology consultant`
- 2016 - 2019 · [Bass Camp](https://www.basscamp.eu) · `Full stack & technology consultant`
- 2013 - 2019 · [CMS.pl](http://cms.pl) · `Web & front-end developer`
- 2009 - 2013 · Freelancer · `Web designer & developer`
- 2003 - 2009 · Freelancer · `Graphic & web designer`

## **Skills** {#skills}
- Languages & Technologies
    - `HTML/SVG` **⚫⚫⚫⚫⚫⚫⚫⚫**
    - `CSS` **⚫⚫⚫⚫⚫⚫⚫⚫**
    - `JavaScript` **⚫⚫⚫⚫⚫⚫⚫**
    - `Node.js` **⚫⚫⚫⚫⚫⚫⚫**
    - `Git` **⚫⚫⚫⚫⚫⚫⚫**
    - `CouchDB` **⚫⚫⚫⚫⚫⚫**
    - `MySQL` **⚫⚫⚫⚫**
    - `PHP` **⚫⚫⚫⚫**
- Libraries & Frameworks
    - `Nuxt.js` **⚫⚫⚫⚫⚫⚫⚫⚫**
    - `Vue.js` **⚫⚫⚫⚫⚫⚫⚫**
    - `Express.js` **⚫⚫⚫⚫⚫⚫**
    - `React` **⚫⚫⚫⚫**
- Software
    - `Fireworks` **⚫⚫⚫⚫⚫⚫⚫⚫**
    - `Illustrator` **⚫⚫⚫⚫⚫⚫**
    - `Photoshop` **⚫⚫⚫⚫**

## **Works** {#works}
- Tenix Maintenance · *2021*
    - application shell · `Nuxt.js, custom Vuetify theme`
    - vehicle & users managment · `GraphQL`
    - custom dashboard graphs and reports · `Chart.js`

- Tenix Vehicle Platform · *2020*
    - application shell · `Nuxt.js, custom Vuetify theme`
    - realtime device monitoring · `STOMP, Websockets`
    - device managment · `REST Api`
    - realtime device terminal · `REST Api`
    - authentication · `OAuth2`

- Tenix Website · *[www.tenix.eu](https://www.tenix.eu/) · 2020*
    - fully responsive and interactive website · `Nuxt.js, CSS`
    - fast hosting with edge CDNs · `Vercel`
    - cloud CMS integration · `Prismic`

- StageEye application · *2020*
    - application shell · `Nuxt.js, Vuetify`
    - realtime device communication · `Websockets`
    - bilateral video and audio streams · `WebRTC`
    - interactive stage drawing and planning · `Touch/Mouse`
    - projects, stages and settings managment · `Firebase`
    - authentication · `Firebase`

- Menago applications · *2019*
    - desktop & mobile app for event managment · `Vue.js, Vuetify, REST Api`

- Karpowicz.pl website · *[www.karpowicz.pl](https://www.karpowicz.pl/) · 2018*
    - fully responsive and interactive website · `Nuxt.js, CSS`
    - custom CMS with products managment · `Vue.js, PouchDB, Buefy`

- Tribe84 Records sales system · *2017*
    - backend app for syncing external sales APIs · `Node.js, CouchDB, REST Apis`
    - front-end app for managing stock & collection (15000+ items) · `AngularJS, PouchDB`

- BASS CAMP ticket sales system · *2016*
    - backend app for selling tickets · `Node.js, CouchDB`
    - mobile app for tickets verifications · `Nuxt.js, Vuetify`

- The Styles Outlets front-end on Drupal 7 · *www.thestyleoutlets.es · 2015*
    - fully responsive and interactive layout · `HTML, LESS, jQuery`
    - interactive centers maps · `JSON, SVG, jQuery`
    - Drupal extension for assigning brands on maps · `jQuery`

- Strefa Piwa website and app · *www.strefa-piwa.pl · 2014* 
    - fully responsive and interactive webpage · `HTML, CSS, jQuery`
    - hybrid mobile application · `AngularJS, Cordova`
    - admin 'on taps' application · `AngularJS, PHP`

- Factory Outlets · *www.factory.pl* · `HTML, CSS, JS`
- DEDE Furniture · *www.dedefurniture.com* · `HTML, CSS, JS`
- Navigator BS · *www.nbs-us.com* · `HTML, CSS, JS`
- EWMD · *www.ewmd.org* · `HTML, CSS, JS`
- Szata · *www.szata.net* · `HTML, CSS, JS`
- Lexus Dealer · *www.lexus-katowice.pl* · `HTML, CSS, JS`

> I agree to the processing of personal data provided in this document for realising the recruitment process pursuant to the Personal Data Protection Act of 10 May 2018 (Journal of Laws 2018, item 1000) and in agreement with Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (General Data Protection Regulation)
